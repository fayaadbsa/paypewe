$(document).ready(function(){
	$("#query").keyup(function(){
		var input = $("#query").val();
		$.ajax({
			url : "/story8/database?q=" + input,
			success : function(data){
				var array_books = data.items;
				$("#list").empty()
				$("#info").empty()
				if (array_books){
					$('#list').append("<thead><tr><th>Cover</th><th>Book's Title</th><th>Publisher</th><th>Author</th></tr></thead>");
					$('#list').append("<tbody>");
					array_books.forEach(addData)
					function addData(item, index) {
						var judul = item.volumeInfo.title;
						var sampul = item.volumeInfo.imageLinks.smallThumbnail;
						var publisher = item.volumeInfo.publisher;
						if (item.volumeInfo.authors){
							var author = item.volumeInfo.authors;
						}else{
							var author = "unknown";
						}
						$('#list').append("<tr><td><img src=" + sampul + "></td><td>" + judul + "</td><td>" + publisher + "</td><td>" + author + "</td></tr>");
					}
					$('#list').append("</tbody>");
				}else{
					$('#info').append("<h1 class=notfound><b>Book's Not Found</b></h1>");
				}
			}
		});
	});
	$("#info").append("<h1><b>Insert A Keyword</b></h1>");
});