from django.contrib import admin
from django.urls import path, include
from story4 import views

app_name = "story4"

urlpatterns = [
   	path('', views.schedule, name='schedule'),
    path('isiform/', views.isiform, name='idisiform'),    
    path('detail-matkul/<int:id>', views.detail, name='iddetail'),
    path('detail-matkul/hapus/<int:id>', views.hapus, name='idhapus'),
]