from django.shortcuts import render

# Create your views here.
def intro(request):
	return render(request, 'intro.html', {})

def portofolio(request):
	return render(request, 'portofolio.html',{})

def error_404(request, exception):
	return render(request, 'error_404.html', {'exception':exception})