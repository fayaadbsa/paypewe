from django.db import models

# Create your models here.
class Activity(models.Model):
	id = models.AutoField(primary_key=True)
	nama = models.CharField(max_length=30, blank=False)

	class Meta:
		ordering = ['nama']

	def __str__(self):
		return self.nama

class Person(models.Model):
	nama = models.CharField(max_length=30, blank=False)
	activity = models.ForeignKey(Activity, on_delete=models.CASCADE)
	
	class Meta:
		ordering = ['nama']

	def __str__(self):
		return self.nama
