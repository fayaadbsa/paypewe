from django.contrib import admin
from django.urls import path, include
from story6 import views

app_name = "story6"

urlpatterns = [
   	path('', views.blank, name='idblank'),
   	path('kegiatan/', views.kegiatan, name='idkegiatan'),
   	path('kegiatanform/', views.kegiatanform, name='idkegiatanform'),
   	path('personform/<int:id>/', views.personform, name='idpersonform'),
]