from django.db import models

# Create your models here.
class Matkul(models.Model):
	
	id = models.AutoField(primary_key=True)
	nama_matkul = models.CharField(max_length=32, blank=False)
	deskripsi = models.TextField(max_length=500, blank=True)
	dosen_pengajar = models.CharField(max_length=32, blank=False)
	ruang_kelas = models.CharField(max_length=32, blank=False)
	sks = models.PositiveSmallIntegerField(default=0)
	kapasitas = models.PositiveSmallIntegerField(default=0)
	semester = models.CharField(max_length=32)

	def __str__(self):
		return self.nama_matkul