# Generated by Django 3.1.1 on 2020-10-17 15:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story4', '0008_auto_20201016_1817'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matkul',
            name='deskripsi',
            field=models.TextField(blank=True, max_length=500),
        ),
    ]
