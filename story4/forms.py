from django import forms
from .models import Matkul

semester_choices = [
		('Gasal 2018/2019', 'Gasal 2018/2019'),
		('Genap 2018/2019', 'Genap 2018/2019'),
		('Gasal 2019/2020', 'Gasal 2019/2020'),
		('Genap 2019/2020', 'Genap 2019/2020'),
		('Gasal 2020/2021', 'Gasal 2020/2021'),
		('Genap 2020/2021', 'Genap 2020/2021'),
	]

class MatkulForm(forms.ModelForm):
	class Meta:
		model = Matkul
		fields = '__all__'
		exclude=['id']

	nama_matkul = forms.CharField(label='Nama Mata Kuliah', required=True, max_length=32, 
		widget = forms.TextInput(attrs={'class':'form-control','type' : 'text', 'placeholder' : 'Nama Mata Kuliah'} ))
	deskripsi = forms.CharField(label='Deskripsi', required=False, max_length=500,
		widget = forms.Textarea(attrs={'class':'form-control','type' : 'text', 'placeholder' : 'Deskripsi Mata Kuliah'} ))
	dosen_pengajar = forms.CharField(label="Dosen Pengajar", required=True, max_length=32,
		widget = forms.TextInput(attrs={'class':'form-control','type' : 'text', 'placeholder' : 'Dosen Pengajar'} ))
	ruang_kelas = forms.CharField(label="Ruang Kelas", required=True, max_length=32, 
		widget = forms.TextInput(attrs={'class':'form-control','type' : 'text', 'placeholder' : 'Ruang Kelas'} ))
	sks = forms.IntegerField(label="Jumlah SKS",min_value=1 ,max_value=10,
		widget = forms.NumberInput(attrs={'class':'form-control','type' : 'number', 'placeholder' : 'Jumlah SKS'} ))
	kapasitas = forms.IntegerField(label="Kapasitas Kelas", min_value=1 ,max_value=200,
		widget = forms.NumberInput(attrs={'class':'form-control','type' : 'number', 'placeholder' : 'Kapasitas Kelas'} ))
	semester = forms.ChoiceField(label="Semester", choices=semester_choices,
		widget = forms.Select(attrs={'class':'form-control','type' : 'select'} ))
