from django.shortcuts import render, redirect
from .models import Activity, Person
from .forms import ActivityForm, PersonForm

# Create your views here.
def blank(request):
	return redirect("story6:idkegiatan")

def kegiatan(request):
	activities = Activity.objects.all()
	persons = Person.objects.all()
	response = {'activities' : activities, 'persons' : persons}
	return render(request, 'kegiatan.html', response)

def kegiatanform(request):
	if (request.method == "POST"):
		form = ActivityForm(request.POST)
		if (form.is_valid):
			form.save()
			return redirect("story6:idkegiatan")

	form = ActivityForm()
	response = {'form' : form }
	return render(request, 'kegiatanform.html', response)

def personform(request, id):
	activity = Activity.objects.get(id=id)
	if (request.method == "POST"):
		form = PersonForm(request.POST)
		if (form.is_valid()):
			data = form.cleaned_data
			activity.person_set.create(nama=data['nama'])
			return redirect("story6:idkegiatan")

	form = PersonForm()
	response = {'form' : form, 'activity' : activity}
	return render(request, 'personform.html', response)