from django.contrib import admin
from django.urls import path, include
from story7 import views

app_name = "story7"

urlpatterns = [
    path('', views.index, name='idindex'),
]