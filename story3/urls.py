from django.contrib import admin
from django.urls import path, include
from story3 import views

app_name = "story3"

urlpatterns = [
#id harus beda, id itu name="id"
    path('', views.intro, name='intro'),
    path('portofolio/', views.portofolio, name='portofolio'),
    path('404/<str:exception>/', views.error_404, name='404'),
]