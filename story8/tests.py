from django.test import TestCase, Client
from django.apps import apps
from .apps import Story8Config

# Create your tests here.
class Story8Test(TestCase):
#app	
	def test_apakah_aplikasi_ada(self):
		self.assertEqual(Story8Config.name, 'story8')
		self.assertEqual(apps.get_app_config('story8').name, 'story8')\

	def test_apakah_url_database_ada(self):
		response =  Client().get('/story8/database/?q=a')
		self.assertEquals(response.status_code, 200)
#url
	def test_apakah_url_story8_ada(self):
		response = Client().get('/story8/')
		self.assertEquals(response.status_code, 200)

#template
	def test_apakah_halaman_story8_memiliki_template(self):
		response =  Client().get('/story8/')
		self.assertTemplateUsed(response, 'bookshelf.html')
