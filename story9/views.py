from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseBadRequest
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .forms import ProfileForm
from .models import Profile
from django.contrib import messages


def index(request):
	return redirect('story3:intro')

def loginsite(request):
	response = {}
	if(request.method == "POST"):
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(request, username=username, password=password)
		if user is not None:
			login(request, user)
			# print("logging in")
			return redirect('story9:profile', username=user.username)
        
		else:
			try:
				user = User.objects.get(username=username)
				messages.error(request, "Wrong Password ❗")
				# print("forgot password ?")
			except:
				messages.error(request, "User not Found ❗")
				# print("user not found")

	return render(request, "masuk.html")


def profile(request, username):
	response = {}
	response['status'] = False
	user = get_object_or_404(User, username=username)
	response['user'] = user
	profile = get_object_or_404(Profile, user=user)
	response['profile'] = profile


	if(request.method == "POST" ):
		if(request.user.is_authenticated and username == request.user.username):
			form = ProfileForm(request.POST)
			if (form.is_valid()):
				name = form.cleaned_data.get('Name')
				profile.name = name
				image = form.cleaned_data.get("Image")
				profile.image = image
				profile.save()
				messages.success(request, "Profile Updated ✨")

			else:
				messages.error(request, "Invalid Input ❗")

	form = ProfileForm(initial={'Name': profile.name, 'Image': profile.image })
	response['form'] = form
	#user adalah dirinya
	if (request.user.is_authenticated and username == request.user.username):
		response['status'] = True
	
	return render(request, "profile.html", response)


def signupsite(request):
	if (request.method == "POST") :
		form = UserCreationForm(request.POST)
		if (form.is_valid()):
			form.save()
			# print("berhasil")
			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=username, password=raw_password)
			profile = Profile.objects.create(user = user)
			profile.name = username
			profile.save()
			login(request, user)
			return redirect("story9:index")

		else:
			username = form.cleaned_data.get('username')
			if username is None : 
				messages.error(request, "Username Already Taken ❗")
				# print("username already taken")
			else:
				messages.error(request, "Invalid Input❗")
				# print("Invalid Input")		
				
	form = UserCreationForm()
	response = {'form' : form}
	return render(request, 'daftar.html', response)

def logoutsite(request):
	logout(request)
	return redirect('story9:index')
