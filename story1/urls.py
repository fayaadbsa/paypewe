from django.contrib import admin
from django.urls import path, include
from story1 import views

app_name = "story1"

urlpatterns = [
    path('', views.index, name='idindex'),
]