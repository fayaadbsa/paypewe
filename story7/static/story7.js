$(document).ready(function(){
  $(".toggle").click(function(){
  	if ($(event.target).is('img')) {
        event.preventDefault();
        return;
    }
    $(event.currentTarget).next().slideToggle("slow");
    
    const $others = $(event.currentTarget).parent().siblings();
    $others.each(function(index, $accordion){
    	$($accordion).find('.isi').slideUp();
    });
  });

  $(".down").click(function(){
  	const $accordion = $(event.currentTarget).parents('.accordion');
  	($accordion).before(($accordion).next());
  });

  $(".up").click(function(){
  	const $accordion = $(event.currentTarget).parents('.accordion');
  	if ($accordion.prev().is('.accordion')){
  		($accordion).after(($accordion).prev());
  	}
  });
});