from django.test import TestCase, Client
from django.apps import apps
from .models import Activity, Person
from .apps import Story6Config

# Create your tests here.
class Tester(TestCase):
	def test_apakah_aplikasi_ada(self):
		self.assertEqual(Story6Config.name, 'story6')
		self.assertEqual(apps.get_app_config('story6').name, 'story6')

#page0
	def test_apakah_halaman_blank_melakukan_redirect(self):
		response =  Client().get('/story6/')
		self.assertEquals(response.status_code, 302)

#page1
	def test_apakah_url_kegiatan_ada(self):
		response = Client().get('/story6/kegiatan/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_halaman_kegiatan_memiliki_template(self):
		response =  Client().get('/story6/kegiatan/')
		self.assertTemplateUsed(response, 'kegiatan.html')

	def test_apakah_di_halaman_kegiatan_ada_teks_KEGIATAN_dan_tombol_tambah(self):
		response =  Client().get('/story6/kegiatan/')
		html_response = response.content.decode('utf8')
		self.assertIn("KEGIATAN", html_response)
		self.assertIn("Tambahkan Kegiatan Baru", html_response)

	def test_apakah_di_halaman_kegiatan_ada_teks_tidak_ada_kegiatan_ketika_tidak_ada_kegiatan(self):
		response =  Client().get('/story6/kegiatan/')
		html_response = response.content.decode('utf8')
		self.assertIn("Kegiatan tidak ditemukan !", html_response)

#page2
	def test_apakah_url_kegiatan_form_ada(self):
		response = Client().get('/story6/kegiatanform/')
		self.assertEquals(response.status_code, 200)

	def test_apakah_halaman_kegiatan_form_memiliki_template(self):
		response =  Client().get('/story6/kegiatanform/')
		self.assertTemplateUsed(response, 'kegiatanform.html')

	def test_apakah_di_halaman_kegiatan_form_ada_teks_FORM_dan_tombol_SUBMIT(self):
		response =  Client().get('/story6/kegiatanform/')
		html_response = response.content.decode('utf8')
		self.assertIn("FORM PENAMBAHAN KEGIATAN BARU", html_response)
		self.assertIn("Nama Kegiatan", html_response)
		self.assertIn("Submit Form", html_response)

	def test_apakah_sudah_ada_model_Activity(self):
		Activity.objects.create(nama="NAMA KEGIATAN")
		banyak_aktivitas = Activity.objects.all().count()
		self.assertEquals(banyak_aktivitas, 1)

	def test_apakah_di_halaman_kegiatan_ada_teks_nama_kegiatan_ketika_ditambahkan(self):
		responsePost = Client().post('/story6/kegiatanform/', {'nama': 'BERENANG'} ) #ini ngetestnya si views.py bukan form yg ada di templates
		responseGet =  Client().get('/story6/kegiatan/')
		html_response = responseGet.content.decode('utf8')
		self.assertIn("BERENANG", html_response)
		self.assertIn("Daftar !", html_response)

#page3
	def test_apakah_url_person_form_ada(self):
		activity = Activity.objects.create(nama="NAMA KEGIATAN")
		response = Client().get('/story6/personform/{0}/'.format(activity.id))
		self.assertEquals(response.status_code, 200)

	def test_apakah_halaman_person_form_memiliki_template(self):
		activity = Activity.objects.create(nama="NAMA KEGIATAN")
		response = Client().get('/story6/personform/{0}/'.format(activity.id))
		self.assertTemplateUsed(response, 'personform.html')

	def test_apakah_sudah_ada_model_Person(self):
		activity = Activity.objects.create(nama="NAMA KEGIATAN")
		Person.objects.create(nama="NAMA PENDAFTAR", activity=activity)
		banyak_pendaftar = Person.objects.all().count()
		self.assertEquals(banyak_pendaftar, 1)

	def test_apakah_di_halaman_kegiatan_ada_teks_nama_pendaftar_ketika_ditambahkan(self):
		activity = Activity.objects.create(nama="NAMA KEGIATAN")
		responsePost = Client().post('/story6/personform/{0}/'.format(activity.id), {'nama': 'NAMA SAYA'} )
		responseGet =  Client().get('/story6/kegiatan/')
		html_response = responseGet.content.decode('utf8')
		self.assertIn("NAMA SAYA", html_response)




