from django.db import models
from django.contrib.auth.models import User;

# Create your models here.
class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	name = models.CharField(max_length = 64, default="", blank=True)
	image = models.CharField(max_length = 500, default = "https://bit.ly/paypewe_profile_pic")