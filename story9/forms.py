from django import forms

class ProfileForm(forms.Form):
	Name = forms.CharField(label='Full Name', required=True, max_length=64,
		widget = forms.TextInput(attrs={'class':'form-control','type' : 'text', 'placeholder' : 'Pay Pewe'} ))
	Image = forms.CharField(label='Image Url', required=False, max_length=500, 
		widget = forms.TextInput(attrs={'class':'form-control','type' : 'text', 'placeholder' : 'https://bit.ly/paypewe_profile_pic'} ))
	