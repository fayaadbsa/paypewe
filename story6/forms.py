from django import forms
from .models import Activity, Person


class ActivityForm(forms.ModelForm):
	class Meta:
		model = Activity
		fields = '__all__'
		exclude=['id']

	nama = forms.CharField(label='Nama Kegiatan', required=True, max_length=32, 
		widget = forms.TextInput(attrs={'class':'form-control','type' : 'text', 'placeholder' : 'Misal: Berenang, Piknik, dll'} ))
	
class PersonForm(forms.ModelForm):
	class Meta:
		model = Person
		fields = '__all__'
		exclude=['id', 'activity']

	nama = forms.CharField(label='Nama Peserta', required=True, max_length=32,  
		widget = forms.TextInput(attrs={'class':'form-control','type' : 'text', 'placeholder' : 'Misal: alfonso de albuquerque'} ))