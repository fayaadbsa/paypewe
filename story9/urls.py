from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from story9 import views

app_name = "story9"

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.loginsite, name='loginsite'),
    path('profile/<str:username>/', views.profile, name='profile'),
    path('logout/', views.logoutsite, name='logoutsite'),
    path('signup/', views.signupsite, name='signupsite'),
]
