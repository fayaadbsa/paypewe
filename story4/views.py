from django.shortcuts import render, redirect
from .models import Matkul
from .forms import MatkulForm

# Create your views here.
def isiform(request):
	if (request.method == "POST") :
		form = MatkulForm(request.POST)
		if (form.is_valid):
			form.save()
			return redirect("story4:schedule")
	form = MatkulForm()
	response = {'form' : form}
	return render(request, 'isiform.html', response)


def schedule(request):
	matkuls = Matkul.objects.all()
	response = {}
	response['matkuls'] = matkuls
	return render(request, 'hasilform.html', response)


def detail(request, id):
	matkul = Matkul.objects.get(id=id)
	response = {'matkul' : matkul}
	return render(request, 'detail-matkul.html', response)

def hapus(request, id):
	matkul = Matkul.objects.get(id=id)
	try:
		matkul.delete()
	except:
		pass
	return redirect("story4:schedule")
