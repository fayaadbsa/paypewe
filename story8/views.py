from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
	return render(request, 'bookshelf.html', {})

def database(request):
	url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
	response = requests.get(url) #response 
	database = response.json()  #convert to json
	return JsonResponse(database, safe = False)