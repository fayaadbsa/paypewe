from django.test import TestCase, Client
from django.apps import apps
from .apps import Story7Config

# Create your tests here.
class Story7Test(TestCase):
#app	
	def test_apakah_aplikasi_ada(self):
		self.assertEqual(Story7Config.name, 'story7')
		self.assertEqual(apps.get_app_config('story7').name, 'story7')
#url
	def test_apakah_url_story7_ada(self):
		response = Client().get('/story7/')
		self.assertEquals(response.status_code, 200)

#template
	def test_apakah_halaman_story7_memiliki_template(self):
		response =  Client().get('/story7/')
		self.assertTemplateUsed(response, 'accordion.html')