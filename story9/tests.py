from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.apps import apps
from .apps import Story9Config
from .models import Profile

# Create your tests here.
class Story9Test(TestCase):
	#apptest
	def test_apakah_aplikasi_ada(self):
		self.assertEqual(Story9Config.name, 'story9')
		self.assertEqual(apps.get_app_config('story9').name, 'story9')\

	#urltest
	def test_apakah_url_story9_melakukan_redirect(self):
		response = Client().get('/story9/')
		self.assertEquals(response.status_code, 302)

	#signuptest
	def test_sign_up_berhasil(self):
		responsePost = Client().post('/story9/signup/', {'username': 'fayaad', 'password1':'teskatasandi',  'password2':'teskatasandi'} ) 
		responseGet =  Client().get('/')
		html_response = responseGet.content.decode('utf8')
		self.assertIn("fayaad", html_response)

	def test_sign_up_gagal_nama_sudah_ada(self):
		User.objects.create_user(username="fayaad", password="katasandilama")
		responsePost = Client().post('/story9/signup/', {'username': 'fayaad', 'password1':'teskatasandi',  'password2':'teskatasandi'} ) 
		html_response = responsePost.content.decode('utf8')
		self.assertIn("Username Already Taken", html_response)

	def test_sign_up_gagal_password_salah(self):
		responsePost = Client().post('/story9/signup/', {'username': 'fayaad', 'password1':'teskatasandi',  'password2':'apasihini'} ) 
		html_response = responsePost.content.decode('utf8')
		self.assertIn("Invalid Input", html_response)

	#logintest
	def test_login_berhasil(self):
		User.objects.create_user(username="udin", password="teskatasandi")
		client = Client()
		responsePost = client.post('/story9/login/', {'username': 'udin', 'password':'teskatasandi'} ) 
		responseGet =  client.get('/')
		html_response = responseGet.content.decode('utf8')
		self.assertIn("udin", html_response)

	def test_login_gagal_nama_tidak_ada(self):
		responsePost = Client().post('/story9/login/', {'username': 'fayaad', 'password':'teskatasandi'} ) 
		html_response = responsePost.content.decode('utf8')
		self.assertIn("User not Found", html_response)

	def test_login_gagal_password_salah(self):
		User.objects.create_user(username="fayaad", password="teskatasandi")
		responsePost = Client().post('/story9/login/', {'username': 'fayaad', 'password':'salahpassword'} ) 
		html_response = responsePost.content.decode('utf8')
		self.assertIn("Wrong Password", html_response)

	#profiletest
	def test_halaman_profile(self):
		responsePost = Client().post('/story9/signup/', {'username': 'aldo', 'password1':'teskatasandi',  'password2':'teskatasandi'} ) 
		response = Client().get('/story9/profile/aldo/')
		html_response = response.content.decode('utf8')
		self.assertIn("aldo", html_response)

	#logouttest
	def test_logout(self):
		User.objects.create_user(username="fayaad", password="teskatasandi")
		responsePost = Client().post('/story9/login/', {'username': 'fayaad', 'password':'teskatasandi'} ) 
		responseGet =  Client().get('/story9/logout/')
		responseGet =  Client().get('/')
		html_response = responseGet.content.decode('utf8')
		self.assertIn("Login", html_response)
		
