from django.contrib import admin
from django.urls import path, include
from story8 import views

app_name = "story8"

urlpatterns = [
    path('', views.index, name='idindex'),
    path('database/', views.database, name='iddatabase'),
]